import sys
import math


def division_pol(div, divs):
    a, b, c, d = [x[0].number for x in div]
    e, f = [x[0].number for x in divs]
    if c * f / e - b * f * f / (e * e) + a * f * f * f / (e * e * e) != d:
        print('The polynomial degree is strictly greater than 2, I can\'t solve.')
        return False
    a, b, c = a / e, (b - a * f / e) / e, d / f
    d = b * b - 4 * a * c
    x1 = -f
    x2 = (-b + pow(d, 1 / 2)) / (2 * a)
    x3 = (-b - pow(d, 1 / 2)) / (2 * a)
    if d < 0:
        print("".join(['Discriminant of polinomial after division is strictly negative, ',
                       'the one rational and two complex root are:\n',
                       x1, '\n',
                       (-b / 2 * a), '+ i * ', pow(-d, 1 / 2) / (2 * a), '\n',
                       (-b / 2 * a), '- i * ', pow(-d, 1 / 2) / (2 * a)]))
    elif d == 0:
        if x1 != x2:
            print("".join(['Discriminant of polinomial after division is equal 0, ',
                           'the two rational root are:\n',
                           str(x1), '\n', str(x2)]))
        else:
            print("".join(['Discriminant of polinomial after division is equal 0, ',
                           'the one rational root are:\n', str(x1)]))
    else:
        print('Discriminant of polinomial after division is strictly positive, the ', end='')
        if len({x1, x2, x3}) == 2:
            print("".join(['two rational root are: \n', str(x2), '\n', str(x3)]))
        else:
            print("".join(['three rational root are: \n', str(x1), '\n', str(x2), '\n', str(x3)]))
    return True


def print_reduce(eq):
    out = []
    for i, x in enumerate(eq):
        out.append(" * ".join([str(i) for i in x]))
    line = " + ".join(out)
    print('Reduced form: ', end='')
    if eq[0][0].number < 0:
        print('-', end='')
    for i in range(len(line)):
        print((line[i] if line[i] != '+' else '-' if line[i + 2] == '-' else '+')
              if line[i] != '-' else '', end='')
    print(' = 0')


def solve3(degs):
    a, b, c, d = degs[3], degs[2], degs[1], degs[0]
    q = (3 * a * c - b ** 2) / (9 * a ** 2)
    r = (9 * a * b * c - 27 * a ** 2 * d - 2 * b ** 3) / (54 * a ** 3)
    rho = (-q ** 3) ** 0.5
    try:
        theta = math.acos(r / rho)
    except (ValueError, TypeError, ZeroDivisionError) as e:
        print('The polynomial degree is strictly greater than 2, I can\'t solve.')
        return False
    s_real = rho ** (1. / 3.) * math.cos(theta / 3)
    t_real = rho ** (1. / 3.) * math.cos(-theta / 3)
    x1 = s_real + t_real - b / (3. * a)
    err = division_pol([[Number(a), Variable('x', 3.0)], [Number(b), Variable('x', 2.0)],
                        [Number(c), Variable('x', 1.0)], [Number(d), Variable('x', 0.0)]],
                       [[Number(1.0), Variable('x', 1.0)], [Number(-x1), Variable('x', 0.0)]])
    return err


def solve2(degs):
    a, b, c = degs[2], degs[1], degs[0]
    d = b * b - 4 * a * c
    if d > 0:
        print('Discriminant is strictly positive, the two solutions are:')
        print((-b + pow(d, 1 / 2)) / (2 * a))
        print((-b - pow(d, 1 / 2)) / (2 * a))
    elif d == 0:
        print('Discriminant is equal 0, the one solutions are:')
        print((-b + pow(d, 1 / 2)) / (2 * a))
    else:
        print('Discriminant is strictly negative, the two solutions are:')
        print((-b / 2 * a), '+ i *', pow(-d, 1 / 2) / (2 * a))
        print((-b / 2 * a), '- i *', pow(-d, 1 / 2) / (2 * a))
    return True


def solve1(degs):
    a, b = degs[1], degs[0]
    print('The solution is:')
    print(-b / a)
    return True


def solve(eq):
    print_reduce(eq)
    var_name = None
    for x in eq:
        for y in x:
            if y.type == 'var':
                if var_name is None:
                    var_name = y.var_name
                elif var_name != y.var_name:
                    print('Error, you try to use more then 1 variable')
                    return False
    if var_name is None:
        if eq[len(eq) - 1][0].number == 0:
            print('Solve of equation is all rational number')
        else:
            print('No solution to the equation')
    else:
        if eq[len(eq) - 1][1].type == 'var':
            degs = {0: 0, 1: 0, 2: 0, 3: 0}
            for x in eq:
                degs[0 if len(x) == 1 else x[1].exp] = x[0].number
            hig_deg = eq[len(eq) - 1][1].exp
            if hig_deg > 3:
                print('Polynomial degree: ', int(hig_deg))
                print('The polynomial degree is strictly greater than 3, I can\'t solve.')
                return True
            elif hig_deg == 3:
                print('Polynomial degree: ', int(hig_deg))
                solve3(degs)
                return True
            elif hig_deg == 2:
                print('Polynomial degree: ', int(hig_deg))
                solve2(degs)
                return True
            elif hig_deg == 1:
                print('Polynomial degree: ', int(hig_deg))
                solve1(degs)
                return True


def comparator(x):
    if x.type == 'num':
        return x.type
    elif x.type == 'var':
        return x.var_name


def comparator2(x):
    if x[len(x) - 1].type == 'num':
        return 0.0
    elif x[len(x) - 1].type == 'var':
        return x[len(x) - 1].exp


def multiply(mlp):
    mlp.sort(key=lambda x: x.type)
    for i in range(len(mlp) - 1):
        if mlp[i].type == 'num':
            if mlp[i + 1].type == 'num':
                mlp[i + 1].number *= mlp[i].number
                mlp[i].number = 1.0
        elif mlp[i].type == 'var':
            if mlp[i + 1].type == 'var' and mlp[i + 1].var_name == mlp[i].var_name:
                mlp[i + 1].exp += mlp[i].exp
                mlp[i] = Number(1.0)
    for i in range(len(mlp)):
        if mlp[i].type == 'var' and mlp[i].exp == 0.0:
            mlp[i] = None
    i = 0
    while i < len(mlp):
        if mlp[i] is None:
            mlp.pop(i)
        else:
            i += 1
    mlp.sort(key=lambda x: x.type)
    i = 0
    while i < len(mlp):
        if mlp[i].type == 'num' and mlp[i].number == 1.0 and i != len(mlp) - 1:
            mlp.pop(i)
        else:
            i += 1
    mlp.sort(key=str)


def add(data):
    data.sort(key=comparator2)
    strings = []
    for x in data:
        s = ''
        for y in x:
            if y.type == 'var':
                s += str(y)
            else:
                s += ''
        strings.append(s)
    for i, x in enumerate(data[:len(data) - 1]):
        if len(data[i]) == len(data[i + 1]) == 1\
                and data[i][0].type == data[i + 1][0].type == 'num':
            data[i + 1][0].number += data[i][0].number
            data[i][0].number = 0.0
    for i, x in enumerate(strings[:len(strings) - 1]):
        if strings[i] == strings[i + 1] and strings[i] != '':
            data[i + 1][0].number += data[i][0].number
            if data[i + 1][0].number == 0:
                data[i + 1][0].number = 0.0
            data[i][0].number = 0.0
    i = 0
    while i < len(data):
        if not data[i] or data[i][0].number == 0.0:
            data.pop(i)
        else:
            i += 1


def parse_multiply(line):
    mlp = line.split("*")
    if len(mlp) == 1 and mlp[0] == '':
        return [Number(1.0)]
    for i, x in enumerate(mlp):
        if x == '':
            print('Error, you try multiply nothing and something else')
            return False
        if x[0].isnumeric() or x[0] == '-':
            num = Number(1.0 if x[0] != '-' else -1.0)
            if not parse_number(x, num):
                return False
            mlp[i] = num
        elif any([x[0] == chr(i) for i in range(97, 123)]):
            name = x[: len(x) if x.find('^') == -1 else x.find('^')]
            var = Variable(name, 0.0)
            if not parse_var(x, var):
                return False
            mlp[i] = var
    multiply(mlp)
    if mlp[0].type == 'var':
        mlp.insert(0, Number(1.0))
    return mlp


def tokenize_sub(line):
    part = []
    err = False
    for i in line:
        if i != ' ':
            part.append(i)
        if all([i != x for x in list('0123456789.qazwsxecdrfvtgbyhnujmikolp*-+^ ')]):
            print('Error, unknown symbol ')
            return False
        elif err and (i == '+' or i == '-'):
            print('Error, unexpected token ', i)
            return False
        elif i == '+' or i == '-':
            err = True
        else:
            err = False
    part = "".join(part)
    subline = [x.strip() for x in part.split('+')]
    subline = [x.split('-') for x in subline]
    data = []
    for y in subline:
        for i, x in enumerate(y):
            if x != '':
                data.append('1.0*' + x if i == 0 else '-1.0*' + x)
    for j, node in enumerate(data):
        mlp = parse_multiply(node)
        if not mlp:
            return False
        data[j] = mlp
    add(data)
    if not data:
        data.append([Number(0.0)])
    return data


def tokenize(line):
    if line.count('=') == 1:
        left, right = line.split('=')
    else:
        print('Error, symbol "=" not found or contains more then 1 times')
        return False
    p0 = tokenize_sub(left)
    if not p0:
        return False
    p1 = tokenize_sub(right)
    if not p1:
        return False
    for i in p1:
        i[0].number = -i[0].number
    p0.extend(p1)
    add(p0)
    if not p0:
        p0.append([Number(0.0)])
    return p0


ar = sys.argv[1:]
if len(ar) > 1:
    if any([x == '-int' for x in ar]):
        from nodes_int import *

        ar.pop(ar.index('-int'))
    else:
        from nodes import *
else:
    from nodes import *
if len(ar) == 0 or any([x == '-help' for x in ar]):
    print('This program can calculate roots of polynomial 1, 2 and 3 degree.\n',
          '\n> ./computor POLYNOMIAL [-int [-help]]\n',
          '\n\t-help\t\tprint this help\n\t-int\t\tuse integer mode.')
    exit(0)
if len(ar) != 1:
    print('Input "', '", "'.join(ar), '" is not recognized, exit.')
    exit(-1)
p = tokenize(ar[0].lower())
if not p:
    exit(-1)
solve(p)
