class Node:
    def evaluate(self):
        raise NotImplementedError()


class Number(Node):
    def __init__(self, number):
        self.number = number
        self.type = 'num'

    def __repr__(self):
        return '\'' + str(int(self.number)) + '\''

    def __str__(self):
        return str(int(self.number))

    def evaluate(self):
        return self.number

    def copy(self):
        return Number(self.number)


class Variable(Node):
    def __init__(self, var_name, exp):
        self.var_name = var_name
        self.exp = exp
        self.type = 'var'

    def __repr__(self):
        return '\'' + self.var_name.upper() + '^' + str(int(self.exp)) + '\''

    def __str__(self):
        return self.var_name.upper() + '^' + str(int(self.exp))

    def evaluate(self):
        return self.var_name

    def copy(self):
        return Variable(self.var_name, self.exp)


class Empty(Node):
    def __init__(self):
        self.type = 'empty'

    def evaluate(self):
        print('Error, can not evaluate Empty node')


def parse_number(line, num):
    try:
        num.number = float(line)
    except ValueError:
        print('Error, can not convert "', line, '" to float')
        return False
    return True


def parse_var(line, value):
    value.exp = 1.0
    exp_i = line.find('^')
    try:
        if exp_i != -1:
            value.exp = float(line[exp_i + 1:])
    except ValueError:
        print('Error, can not convert "', line[exp_i:], '" to float')
        return False
    return True
