## Computor v1

School 21 (Ecole 42) students project, realise simple reduser of polynomial and solver (for degree less then 4).
Program computorv1.py takes the following flags as parameters:

```
    polynomial  polynomial to reduse and solve
    -help       print help
    -int        output the integer numbers at the reduced form
```

File `computorv1` simple bash wrapper for using in the same way as in the subject examples.

This version is oversimplify and work only with '+', '-' and '*', also you can use '^' to set variable degree (only for variable, exponent is number). Next project along this route will be implement soon.

## Subject
[Subject](https://cdn.intra.42.fr/pdf/pdf/13223/en.subject.pdf)
